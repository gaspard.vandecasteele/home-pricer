# frozen_string_literal: true

require 'capybara'
require 'selenium/webdriver'

# Standard configuration for local run on RackTest
# ---------------------------------------------------------------------

Capybara.server = :puma, { Silent: true }
Capybara.app_host = 'http://localhost:3000'
Capybara.run_server = false
